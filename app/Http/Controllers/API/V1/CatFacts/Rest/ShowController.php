<?php

namespace App\Http\Controllers\API\V1\CatFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\ShowController as BaseController;
use App\Models\CatFact;

class ShowController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new CatFact());
    }
}
