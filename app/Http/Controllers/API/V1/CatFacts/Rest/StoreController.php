<?php

namespace App\Http\Controllers\API\V1\CatFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\StoreController as BaseController;
use App\Http\Requests\API\V1\CatFacts\Rest\StoreRequest;
use App\Models\CatFact;
use Symfony\Component\HttpFoundation\Response;

class StoreController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new CatFact(), 'Cat');
    }

    public function __invoke(StoreRequest $request): Response
    {
        return parent::baseInvoke($request);
    }
}
