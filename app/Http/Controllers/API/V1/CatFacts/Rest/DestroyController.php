<?php

namespace App\Http\Controllers\API\V1\CatFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\DestroyController as BaseController;
use App\Models\CatFact;

class DestroyController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new CatFact(), 'Cat');
    }
}
