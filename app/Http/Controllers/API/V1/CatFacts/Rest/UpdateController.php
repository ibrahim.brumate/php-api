<?php

namespace App\Http\Controllers\API\V1\CatFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\UpdateController as BaseController;
use App\Http\Requests\API\V1\CatFacts\Rest\UpdateRequest;
use App\Models\CatFact;
use Symfony\Component\HttpFoundation\Response;

class UpdateController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new CatFact(), 'Cat');
    }

    public function __invoke(int $id, UpdateRequest $request): Response
    {
        return parent::baseInvoke($id, $request);
    }
}
