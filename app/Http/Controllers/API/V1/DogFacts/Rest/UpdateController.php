<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\UpdateController as BaseController;
use App\Http\Requests\API\V1\DogFacts\Rest\UpdateRequest;
use App\Models\DogFact;
use Symfony\Component\HttpFoundation\Response;

class UpdateController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new DogFact(), 'Dog');
    }

    public function __invoke(int $id, UpdateRequest $request): Response
    {
        return parent::baseInvoke($id, $request);
    }
}
