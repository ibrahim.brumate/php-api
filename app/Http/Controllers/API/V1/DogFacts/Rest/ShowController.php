<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\ShowController as BaseController;
use App\Models\DogFact;

class ShowController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new DogFact());
    }
}
