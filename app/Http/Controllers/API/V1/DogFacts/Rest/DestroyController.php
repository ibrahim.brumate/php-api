<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\DestroyController as BaseController;
use App\Models\DogFact;

class DestroyController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new DogFact(), 'Dog');
    }
}
