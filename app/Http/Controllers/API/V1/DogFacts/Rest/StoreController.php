<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\API\V1\BaseFacts\Rest\StoreController as BaseController;
use App\Http\Requests\API\V1\DogFacts\Rest\StoreRequest;
use App\Models\DogFact;
use Symfony\Component\HttpFoundation\Response;

class StoreController extends BaseController
{
    public function __construct()
    {
        parent::__construct(new DogFact(), 'Cat');
    }

    public function __invoke(StoreRequest $request): Response
    {
        return parent::baseInvoke($request);
    }
}
