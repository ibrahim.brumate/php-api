<?php

namespace App\Http\Controllers\API\V1\BaseFacts\Rest;

use App\Http\Controllers\Controller;
use App\Repositories\BaseFactRepository;
use Illuminate\Database\Eloquent\Model;
use Exception;
use App\Http\Requests\API\V1\CatFacts\Rest\StoreRequest as CatRequest;
use App\Http\Requests\API\V1\DogFacts\Rest\StoreRequest as DogRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreController extends Controller
{
    protected BaseFactRepository $repository;
    protected $modelName;

    public function __construct(Model $model, $modelName)
    {
        $this->repository = new BaseFactRepository($model);
        $this->modelName = $modelName;
    }

    public function baseInvoke(CatRequest|DogRequest $request): Response
    {
        try {
            $baseFact = $this->repository->store($request->all());
        } catch (Exception $exception) {
            abort(500, $exception->getMessage());
        }

        if (!$baseFact) {
            abort(500, $this->modelName . ' fact could not be saved');
        }

        return response()->json($baseFact);
    }
}
