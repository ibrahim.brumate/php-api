<?php

namespace App\Http\Controllers\API\V1\BaseFacts\Rest;

use App\Http\Controllers\Controller;
use App\Repositories\BaseFactRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Response;

class DestroyController extends Controller
{
    protected BaseFactRepository $repository;
    protected $modelName;

    public function __construct(Model $model, $modelName)
    {
        $this->repository = new BaseFactRepository($model);
        $this->modelName = $modelName;
    }

    public function __invoke(int $id): Response
    {
        $baseFact = $this->repository->getById($id);

        if (!$baseFact) {
            abort(404);
        }

        try {
            $deleted = $this->repository->destroy($baseFact);
        } catch (Exception $exception) {
            abort(500, $exception->getMessage());
        }

        if (!$deleted) {
            abort(500, $this->modelName . ' fact could not be deleted');
        }

        return response()->json(null, 204);
    }
}
