<?php

namespace App\Http\Controllers\API\V1\BaseFacts\Rest;

use App\Http\Controllers\Controller;
use App\Repositories\BaseFactRepository;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    protected BaseFactRepository $repository;

    public function __construct(Model $model)
    {
        $this->repository = new BaseFactRepository($model);
    }

    public function __invoke(Request $request): Response
    {
        $baseFacts = $this->repository->getAllWithPagination($request->all());

        return response()->json($baseFacts);
    }
}
