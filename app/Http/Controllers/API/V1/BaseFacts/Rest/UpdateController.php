<?php

namespace App\Http\Controllers\API\V1\BaseFacts\Rest;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\CatFacts\Rest\UpdateRequest as CatUpdateRequest;
use App\Http\Requests\API\V1\DogFacts\Rest\UpdateRequest as DogUpdateRequest;
use App\Repositories\BaseFactRepository;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class UpdateController extends Controller
{
    protected BaseFactRepository $repository;
    protected $modelName;

    public function __construct(Model $model, $modelName)
    {
        $this->repository = new BaseFactRepository($model);
        $this->modelName = $modelName;
    }

    public function baseInvoke(int $id, CatUpdateRequest|DogUpdateRequest $request): Response
    {
        $baseFact = $this->repository->getById($id);

        if (!$baseFact) {
            abort(404);
        }

        try {
            $updated = $this->repository->update($baseFact, $request->all());
        } catch (Exception $exception) {
            abort(500, $exception->getMessage());
        }

        if (!$updated) {
            abort(500, $this->modelName . ' fact could not be updated');
        }

        return response()->json($updated);
    }
}
