<?php

namespace App\Http\Controllers\API\V1\BaseFacts\Rest;

use App\Http\Controllers\Controller;
use App\Repositories\BaseFactRepository;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Response;

class ShowController extends Controller
{
    protected BaseFactRepository $repository;

    public function __construct(Model $model)
    {
        $this->repository = new BaseFactRepository($model);
    }

    public function __invoke(int $id): Response
    {
        $baseFact = $this->repository->getById($id);

        if (!$baseFact) {
            abort(404);
        }

        return response()->json($baseFact);
    }
}
