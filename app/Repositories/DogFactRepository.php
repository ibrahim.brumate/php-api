<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Repositories\BaseFactRepository;
use App\Models\DogFact;

class DogFactRepository extends BaseFactRepository
{
    public function __construct()
    {
        parent::__construct(new DogFact());
    }
}
