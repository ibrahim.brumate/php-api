<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class BaseFactRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAllWithPagination(array $options = [], array $relations = []): ?LengthAwarePaginator
    {
        $pageSize = $options['pageSize'] ?? config('pagination.default_size');
        $order = $options['order'] ?? config('order.default_direction');

        return $this->model->orderBy('id', $order)->with($relations)->paginate($pageSize);
    }

    public function getById(int $id, array $relations = [])
    {
        return $this->model->with($relations)->find($id);
    }

    public function store(array $options)
    {
        $fact = new $this->model();

        return $this->setFieldsAndPersist($fact, $options);
    }

    public function update($fact, array $options)
    {
        return $this->setFieldsAndPersist($fact, $options);
    }

    protected function setFieldsAndPersist($fact, array $options)
    {
        $fact->fact = $options['fact'];

        if ($fact->save()) {
            return $fact;
        }

        return null;
    }

    public function destroy($fact): bool
    {
        return $fact->delete();
    }
}
