<?php

return [
    'default_direction' => env('QUERY_ORDER_DEFAULT_DIRECTION', 'ASC'),
];
